import { Container } from 'react-bootstrap';
import Home from './pages/Home';
import AppNavBar from './components/AppNavBar';
import Particle from './components/Particle';


// stylesheet
import './App.css';
import 'animate.css'

// fonts
import "./assets/fonts/MavenPro-Regular.ttf";
import "./assets/fonts/MavenPro-Medium.ttf";
import "./assets/fonts/MavenPro-Bold.ttf";


function App() {
  return (
    <>
    <AppNavBar />
    <Container>
      <Home />
    </Container>
    <Particle />
    </>
  );
};

export default App;
