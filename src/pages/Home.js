import Banner from '../components/Banner';
import Projects from '../components/Projects';
import Contact from '../components/Contact';


export default function Home(){
    return (
        <>
            <main id='home' className='my-5'>
                <Banner />
            </main>
            <section id='projects' className='my-5'>
                <Projects/>
            </section>
            <section id='contact' className='my-5'>
                <Contact />
            </section>
            <footer className='text-center pt-5'>
                <p className='mb-0'>Copyright © 2023 Earl John Mambinunsad • Full Stack Developer</p>
            </footer>
        </>
    )
};