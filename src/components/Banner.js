import { Row, Col, Image } from 'react-bootstrap';
import Me from '../assets/img/me.png'
import ModalContact from './ModalContact';


export default function Banner(){
    
    
    return( 
        <Row className='banner-row'>
            <Col md={7} className='banner-col mt-auto animate__animated animate__slideInLeft'>
                <a href='#home' className='banner-t btn-about d-block'><span>Hi! I'm</span></a>
                <a href='#projects' className='banner-t btn-projects d-block'><span>Earl John</span></a>
                <a href='#contact' className='banner-t btn-contact d-block'><span>Full Stack Developer</span></a>
                <p className='banner-p' >
                     A Filipino full-stack developer who likes to create functional and responsive websites. I had held a position as a virtual assistant on a small company in ph, but I left to seek a career in the IT sector. I had a fondness for coding, so I made the decision to study whenever I had free time while working as a freelancer, and as I was surfing Facebook, I spotted adverts for Zuitt, one of the best coding bootcamps in the Philippines. I made an attempt to apply, and I was granted admission as a student. Luckily ive been choose as the Best in MERN-Stack Developer in my batch.
                </p>
                <ModalContact />
            </Col>
            <Col md={5} className="text-end animate__animated animate__zoomIn animate_delay-5s d-md-block  d-none">
                <Image className='banner-me' src={Me} fluid />
            </Col>
        </Row>
    )
};