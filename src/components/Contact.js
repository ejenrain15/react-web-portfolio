import { useState } from "react";
import { Row, Col, Form, Button, Image } from "react-bootstrap";
import Swal from 'sweetalert2';
import Nav1 from '../assets/img/nav-icon1.svg';
import Nav2 from '../assets/img/nav-icon2.svg';



export default function Contact (){
    const formInitialDetails = {
        name : "",
        email: "",
        message: ""
    }
    
    const [formDetails, setFormDetails] = useState(formInitialDetails);
    const [buttonText, setButtonText] = useState("Send")

    const onFormUpdate = (category, value) => {
        setFormDetails({
            ...formDetails,
            [category] : value
        })
    }

    const handleSubmit = async (e) => {
        try {
            e.preventDefault()
            setButtonText("Sending...")
            const response = await  fetch(`${process.env.REACT_APP_API_URL}/messages/send`, {
                method: "POST",
                headers : {
                    'Content-Type': "application/json"
                },
                body: JSON.stringify({
                    fullName: formDetails.name,
                    email: formDetails.email,
                    message: formDetails.message
                })
            })
    
            const data  = await response.json()
            if(data){
                Swal.fire({
                    title: "Successfully Sent",
                    icon: "success",
                    text: "Thank you for contacting me i will get back to you later."
                })
                setButtonText("Send");
                setFormDetails(formInitialDetails);
            }else {
                Swal.fire({
                    title: "Something Went Wrong",
                    icon: "error",
                    text: "Please try again later."
                })
                setButtonText("Send")
            }
        }catch(error) {
            console.log(error)
        }
    }
 
    return (
        <Row className="pt-5">
            <Col lg={12} className="text-center my-5">
                <h1 className="contact-t">GET IN TOUCH</h1>
            </Col>
            <Col lg={6} md={6} className="px-5 mt-5">
                <Form onSubmit={(e) => handleSubmit(e)} className="contact-form">
                    <Form.Control className="contact-i text-white" type="text" value={formDetails.name} placeholder="Name" onChange={(e) => onFormUpdate('name', e.target.value)} required />
                    <Form.Control className="contact-i text-white my-3" type="email" value={formDetails.email} placeholder="Email" onChange={(e) => onFormUpdate('email', e.target.value)} required />
                    <Form.Control className="contact-i text-white mb-3" as="textarea" rows={6} value={formDetails.message} placeholder="Message" onChange={(e) => onFormUpdate('message', e.target.value)} required />
                    <div className="text-center">
                        <Button className="contact-form-btn" type="submit" variant="outline-info">{buttonText}</Button>
                    </div>
                </Form>
            </Col>
            <Col lg={6} md={6} className="text-center mt-5">
                <h5 className="mt-4">General Trias, Cavite, Philippines</h5>
                <h5 className="my-4">+(639)-907-0990-165</h5>
                <h5>mambinunsad.earljohn@gmail.com</h5>
                <a className="contact-a d-block my-4" href="https://www.linkedin.com/in/earl-john-mambinunsad-433565233/" target="_blank" ><Image className="nav-icon p-2" src={Nav1} alt="LinkedIn Img"/> LinkedIn</a>
                <a className="contact-a d-block" href="https://www.facebook.com/03Meg29/" target="_blank" ><Image className="nav-icon p-2" src={Nav2} alt="Facebook Img"/> Facebook</a>
            </Col>
        </Row>
    )
}