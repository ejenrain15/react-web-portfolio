import { Container, Nav, Navbar, Image } from 'react-bootstrap';
import Logo from '../assets/img/logo.png';
import Icon1 from '../assets/img/nav-icon1.svg';
import Icon2 from '../assets/img/nav-icon2.svg';


export default function AppNavBar(){
    return (
        <Navbar className='navbar' bg="dark" expand="lg" sticky='top'>
            <Container fluid className='mx-3'>
                <Navbar.Brand href="#home">
                    <Image className='nav-logo' src={Logo} fluid />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" className='bg-light' />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">
                    <Nav.Link className= 'nav-text py-3' href="#home">Home</Nav.Link>
                    <Nav.Link className= 'nav-text py-3' href="#projects">Projects</Nav.Link>
                    <Nav.Link className= 'nav-text py-3' href="#contact">Contact</Nav.Link>
                    <Nav.Link className='py-2' href="#https://www.linkedin.com/in/earl-john-mambinunsad-433565233/" target="_blank">
                        <Image className='nav-icon p-2' src={Icon1} fluid />
                    </Nav.Link>
                    <Nav.Link className='py-2' href="https://www.facebook.com/03Meg29/" target="_blank">
                        <Image className='nav-icon p-2' src={Icon2} fluid />
                    </Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
};