import { Row, Col, Card, Button, Carousel } from 'react-bootstrap';
import Work1 from '../assets/img/work-1.jpg';
import Work1two from '../assets/img/work-1-2.png';
import Work1three from '../assets/img/work-1-3.png';
import Work2 from '../assets/img/work-2.jpg';
import Work2two from '../assets/img/work-2-2.png';
import Work2three from '../assets/img/work-2-3.png';
import Work3 from '../assets/img/work-3.png';
import Work3two from '../assets/img/work-3-2.png';
import Work3three from '../assets/img/work-3-3.png';



export default function Projects(){
    return (
        <>

            <Row className='text-center justify-content-center align-items-center pt-5'>

                <Col lg={12} className="m-5">
                    <h1 className='projects-t'>PROJECTS</h1>
                </Col>

                <Col lg={4} md={8} className="projects-col mt-5">
                   <Card className='projects-card p-3'>
                        {/* <Card.Img className='projects-card-img' variant="top" src={Work1} fluid /> */}
                        <Carousel>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work1}
                                alt="First slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work1two}
                                alt="Second slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work1three}
                                alt="Third slide"
                                />
                            </Carousel.Item>
                        </Carousel>
                        <Card.Body>
                            <Card.Title className='projects-card-t mb-3'>STATIC WEB PORTFOLIO</Card.Title>
                            <Card.Text className='projects-card-p mb-4'>
                            A Static Web Portfolio made from HTML, CSS and BOOTSTRAP.
                            </Card.Text>
                            <Button className='me-2' variant="outline-info" href="https://ejenrain15.github.io/webportfolio/" target='_blank'>Web APP</Button>
                            <Button variant="outline-danger" href='https://gitlab.com/b224-mambinunsad/capstone-project/capstone-project' target='_blank'>Code</Button>
                        </Card.Body>
                    </Card>
                </Col>

                <Col lg={4} md={8} className="projects-col mt-5">
                   <Card className='projects-card p-3'>
                        {/* <Card.Img className='projects-card-img' variant="top" src={Work2} fluid /> */}
                        <Carousel>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work2}
                                alt="First slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work2two}
                                alt="Second slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work2three}
                                alt="Third slide"
                                />
                            </Carousel.Item>
                        </Carousel>
                        <Card.Body>
                            <Card.Title className='projects-card-t mb-3'>E-COMMERCE API</Card.Title>
                            <Card.Text className='projects-card-p mb-4'>
                            A E-commerce API made from Node.js, Express.js, MongoDB, POSTMAN. 
                            </Card.Text>
                            <Button className='me-2' variant="outline-info" href='https://documenter.getpostman.com/view/24279080/2s8YzTUhuJ#9621fb30-4945-4a66-bdb9-4f61f50ff28f' target='_blank'>Web APP</Button>
                            <Button variant="outline-danger" href='https://gitlab.com/b224-mambinunsad/s42-s46/ecommerce-api-capstone' target='_blank'>Code</Button>
                        </Card.Body>
                    </Card>
                </Col>

                <Col lg={4} md={8} className="projects-col mt-5">
                   <Card className='projects-card p-3'>
                        {/* <Card.Img className='projects-card-img' variant="top" src={Work3} fluid /> */}
                        <Carousel>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work3}
                                alt="First slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work3two}
                                alt="Second slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="projects-card-img d-block w-100"
                                src={Work3three}
                                alt="Third slide"
                                />
                            </Carousel.Item>
                        </Carousel>

                        <Card.Body>
                            <Card.Title className='projects-card-t mb-3'>E-COMMERCE APP</Card.Title>
                            <Card.Text className='projects-card-p mb-4'>
                            A E-commerce APP made from MongoDB, Express.js, React.js, Node.js.
                            </Card.Text>
                            <Button className='me-2' variant="outline-info" href='https://e-commerce-react-silk.vercel.app/' target='_blank'>Web APP</Button>
                            <Button variant="outline-danger" href='https://gitlab.com/b224-mambinunsad/s59-s64/e-commerce-react' target='_blank'>Code</Button>
                        </Card.Body>
                    </Card>
                </Col>

            </Row>
        </>
    )
}