import { useState } from "react";
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ModalContact(){
    
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const formInitialDetails = {
    name : "",
    email: "",
    message: ""
}

const [formDetails, setFormDetails] = useState(formInitialDetails);
const [buttonText, setButtonText] = useState("Send")

const onFormUpdate = (category, value) => {
    setFormDetails({
        ...formDetails,
        [category] : value
    })
}

const handleSubmit = async (e) => {
    try {
        e.preventDefault()
        setButtonText("Sending...")
        const response = await  fetch(`${process.env.REACT_APP_API_URL}/messages/send`, {
            method: "POST",
            headers : {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                fullName: formDetails.name,
                email: formDetails.email,
                message: formDetails.message
            })
        })

        const data  = await response.json()
        if(data){
            Swal.fire({
                title: "Successfully Sent",
                icon: "success",
                text: "Thank you for contacting me i will get back to you later."
            })
            setButtonText("Send");
            setFormDetails(formInitialDetails);
            handleClose()
        }else {
            Swal.fire({
                title: "Something Went Wrong",
                icon: "error",
                text: "Please try again later."
            })
            setButtonText("Send")
        }
    }catch(error) {
        console.log(error)
    }
}

  return (
    <>
      <Button variant='outline-info' onClick={handleShow}>Contact Me</Button>

      <Modal 
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title className="text-dark">Contact Me</Modal.Title>
        </Modal.Header>
        <Modal.Body className="text-dark">
                <Form onSubmit={(e) => handleSubmit(e)} className="contact-form">
                    <Form.Control className="contact-i text-dark" type="text" value={formDetails.name} placeholder="Name" onChange={(e) => onFormUpdate('name', e.target.value)} required />
                    <Form.Control className="contact-i text-dark my-3" type="email" value={formDetails.email} placeholder="Email" onChange={(e) => onFormUpdate('email', e.target.value)} required />
                    <Form.Control className="contact-i text-dark mb-3" as="textarea" rows={6} value={formDetails.message} placeholder="Message" onChange={(e) => onFormUpdate('message', e.target.value)} required />
                    <div className="text-center">
                        <Button className="contact-form-btn" type="submit" variant="outline-info">{buttonText}</Button>
                    </div>
                </Form>
        </Modal.Body>
      </Modal>
    </>
  )
}